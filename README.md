#  Mari Belajar Python

Merupakan tutorial belajar Python yang diambil dari berbagai sumber.

Python merupakan bahasa pemrograman yang diciptakan oleh Guido van Rossum pada tahun 1991. Python termasuk dalam kategori *high-level programming* dikarenakan bahasa pemrograman Python mudah dituliskan dan dibaca oleh manusia. Bahasa pemrograman ini sifatnya *open source* dan digunakan untuk banyak hal seperti pembuatan/pengembangan aplikasi mobile dan web, data science, artificial intelligence, dll (*general-purpose programming language*)
